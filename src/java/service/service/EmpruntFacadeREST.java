/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.service;

import entity.Emprunt;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 *
 * @author Tom
 */
@Stateless
@Path("emprunts")
public class EmpruntFacadeREST extends AbstractFacade<Emprunt> {
    @PersistenceContext(unitName = "MediaNet-ServicesPU")
    private EntityManager em;

    public EmpruntFacadeREST() {
        super(Emprunt.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Emprunt entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Emprunt entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Emprunt find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Emprunt> findAllByAdherent(@QueryParam("filter") String filter) {
        //utiliser ?filter=type::1;genre::3 par exemple au lieu de ?type=1&genre=3 (best practice REST)
        if(filter==null)
            return super.findAll();
        Integer id =null;
        Integer emprunte = null;
        String[] params = filter.split(";");
        HashMap<String, String> paramListe = new HashMap<>();
        for (String param : params){
            String[] tmp = param.split("::");
            if(tmp.length==2){
                String nomParam = tmp[0];
                if(nomParam.equals("idAdherent")){
                    paramListe.put(nomParam, tmp[1]);
                }else if(nomParam.equals("emprunte")){
                    paramListe.put(nomParam, tmp[1]);
                    try{
                        emprunte = Integer.parseInt(tmp[1]);
                    }catch(Exception e){
                        
                    }
                }
            }
        }
            String query = "SELECT e FROM Emprunt e";
            int count = 0;
            for(String key : paramListe.keySet()) {
                
                if(count == 0) {
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }
                
                switch (key) {
                    case "idAdherent":
                        query += "e.adherentId.idAdherent = :idAdherent";
                        break;
                    case "emprunte":
                        if(emprunte != null){
                            if(emprunte == 1)
                                query += "e.dateRetour IS NOT NULL";
                            else
                                query += "e.dateRetour IS NULL";
                        }
                        break;
                    default:
                        break;
                }
                count++;
            }
                
            count = 1;
            Query q = em.createQuery(query);
                
            try {
                for(String key : paramListe.keySet()) {
                    switch (key) {
                        case "idAdherent":
                            q.setParameter("idAdherent", Integer.parseInt(paramListe.get(key)));
                            break;
                        case "emprunte":
                            if(emprunte != null){
                                count--;
                            }
                        break;
                        default:
                            break;
                    }
                    count++;
                }
            } catch (NumberFormatException e) {
                return null;
            }
            return q.getResultList();
        
    }
    

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Emprunt> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
