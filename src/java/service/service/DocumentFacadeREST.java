/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.service;

import entity.Document;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 *
 * @author Tom
 */
@Stateless
@Path("documents")
public class DocumentFacadeREST extends AbstractFacade<Document> {
    @PersistenceContext(unitName = "MediaNet-ServicesPU")
    private EntityManager em;

    public DocumentFacadeREST() {
        super(Document.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Document entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Document entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Document find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    /*@GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Document> findAll() {
        return super.findAll();
    }*/
    
    
    @GET
    @Produces({"application/xml", "application/json"})
    public List<Document> findAllByParam(@QueryParam("filter") String filter) {
        if(filter==null)
            return super.findAll();
        
        String[] params = filter.split(";");
        HashMap<String, String> paramListe = new HashMap<>();
        for (String param : params){
            String[] tmp = param.split("::");
            //"document", "genre", "type", "disponible", "user"
            if(tmp.length==2){
                String nomParam = tmp[0];
                if (nomParam.equals("document") || nomParam.equals("genre") || nomParam.equals("type") || nomParam.equals("user") ){
                    paramListe.put(nomParam, tmp[1]);
                }
            }
        }
            String query = "SELECT d FROM Document d";
            int count = 0;
            for(String key : paramListe.keySet()) {
                
                if(count == 0) {
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }
                
                switch (key) {
                    case "document":
                        query += "(UPPER(d.titre) LIKE :doc OR UPPER(d.descriptionDocument) LIKE :doc)";
                        break;
                    case "genre":
                        query += "d.genreId.idGenre = :genreId";
                        break;
                    case "type":
                        query += "d.typeId.idType = :typeId";
                        break;
                    /*case "user":
                        query += "d.userId = :userId";
                        break;*/
                    default:
                        break;
                }
                count++;
            }
                
            count = 1;
            Query q = em.createQuery(query);
                
            try {
                for(String key : paramListe.keySet()) {
                    switch (key) {
                        case "document":
                            q.setParameter("doc", "%"+paramListe.get(key).toUpperCase()+"%");
                            break;
                        /*case "user":
                            q.setParameter("userId", paramListe.get(key));
                            break;*/
                        case "genre":
                            q.setParameter("genreId", Integer.parseInt(paramListe.get(key)));
                            break;
                        case "type":
                            q.setParameter("typeId", Integer.parseInt(paramListe.get(key)));
                            break;
                        default:
                            break;
                    }
                    count++;
                }
            } catch (NumberFormatException e) {
                return null;
            }
            return q.getResultList();
        
    }
    

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Document> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
