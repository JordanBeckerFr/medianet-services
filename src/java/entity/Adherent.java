/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Tom
 */
@Entity
@Table(name = "ADHERENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adherent.findAll", query = "SELECT a FROM Adherent a"),
    @NamedQuery(name = "Adherent.findByIdAdherent", query = "SELECT a FROM Adherent a WHERE a.idAdherent = :idAdherent"),
    @NamedQuery(name = "Adherent.findByNom", query = "SELECT a FROM Adherent a WHERE a.nom = :nom"),
    @NamedQuery(name = "Adherent.findByPrenom", query = "SELECT a FROM Adherent a WHERE a.prenom = :prenom"),
    @NamedQuery(name = "Adherent.findByMail", query = "SELECT a FROM Adherent a WHERE a.mail = :mail"),
    @NamedQuery(name = "Adherent.findByDateAdhesion", query = "SELECT a FROM Adherent a WHERE a.dateAdhesion = :dateAdhesion"),
    @NamedQuery(name = "Adherent.findByAdresse", query = "SELECT a FROM Adherent a WHERE a.adresse = :adresse")})
public class Adherent implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ADHERENT")
    private Integer idAdherent;
    @Size(max = 255)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 255)
    @Column(name = "PRENOM")
    private String prenom;
    @Size(max = 255)
    @Column(name = "MAIL")
    private String mail;
    @Column(name = "DATE_ADHESION")
    @Temporal(TemporalType.DATE)
    private Date dateAdhesion;
    @Size(max = 255)
    @Column(name = "ADRESSE")
    private String adresse;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adherentId")
    private Collection<Emprunt> empruntCollection;

    public Adherent() {
    }

    public Adherent(Integer idAdherent) {
        this.idAdherent = idAdherent;
    }

    public Integer getIdAdherent() {
        return idAdherent;
    }

    public void setIdAdherent(Integer idAdherent) {
        this.idAdherent = idAdherent;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getDateAdhesion() {
        return dateAdhesion;
    }

    public void setDateAdhesion(Date dateAdhesion) {
        this.dateAdhesion = dateAdhesion;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @XmlTransient
    public Collection<Emprunt> getEmpruntCollection() {
        return empruntCollection;
    }

    public void setEmpruntCollection(Collection<Emprunt> empruntCollection) {
        this.empruntCollection = empruntCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAdherent != null ? idAdherent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adherent)) {
            return false;
        }
        Adherent other = (Adherent) object;
        if ((this.idAdherent == null && other.idAdherent != null) || (this.idAdherent != null && !this.idAdherent.equals(other.idAdherent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Adherent[ idAdherent=" + idAdherent + " ]";
    }
    
}
