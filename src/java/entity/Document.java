/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Tom
 */
@Entity
@Table(name = "DOCUMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Document.findAll", query = "SELECT d FROM Document d"),
    @NamedQuery(name = "Document.findByIdDocument", query = "SELECT d FROM Document d WHERE d.idDocument = :idDocument"),
    @NamedQuery(name = "Document.findByTitre", query = "SELECT d FROM Document d WHERE d.titre = :titre"),
    @NamedQuery(name = "Document.findByAuteur", query = "SELECT d FROM Document d WHERE d.auteur = :auteur"),
    @NamedQuery(name = "Document.findByDescriptionDocument", query = "SELECT d FROM Document d WHERE d.descriptionDocument = :descriptionDocument"),
    @NamedQuery(name = "Document.findByUrl", query = "SELECT d FROM Document d WHERE d.url = :url"),
    @NamedQuery(name = "Document.findByDateAjout", query = "SELECT d FROM Document d WHERE d.dateAjout = :dateAjout"),
    @NamedQuery(name = "Document.findByDatePublication", query = "SELECT d FROM Document d WHERE d.datePublication = :datePublication")})
public class Document implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DOCUMENT")
    private Integer idDocument;
    @Size(max = 255)
    @Column(name = "TITRE")
    private String titre;
    @Size(max = 255)
    @Column(name = "AUTEUR")
    private String auteur;
    @Size(max = 255)
    @Column(name = "DESCRIPTION_DOCUMENT")
    private String descriptionDocument;
    @Size(max = 255)
    @Column(name = "URL")
    private String url;
    @Column(name = "DATE_AJOUT")
    @Temporal(TemporalType.DATE)
    private Date dateAjout;
    @Column(name = "DATE_PUBLICATION")
    @Temporal(TemporalType.DATE)
    private Date datePublication;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentId")
    private Collection<Emprunt> empruntCollection;
    @JoinColumn(name = "ETAT_ID", referencedColumnName = "ID_ETAT")
    @ManyToOne(optional = false)
    private Etat etatId;
    @JoinColumn(name = "GENRE_ID", referencedColumnName = "ID_GENRE")
    @ManyToOne(optional = false)
    private Genre genreId;
    @JoinColumn(name = "TYPE_ID", referencedColumnName = "ID_TYPE")
    @ManyToOne(optional = false)
    private Type typeId;

    public Document() {
    }

    public Document(Integer idDocument) {
        this.idDocument = idDocument;
    }

    public Integer getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(Integer idDocument) {
        this.idDocument = idDocument;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getDescriptionDocument() {
        return descriptionDocument;
    }

    public void setDescriptionDocument(String descriptionDocument) {
        this.descriptionDocument = descriptionDocument;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    @XmlTransient
    public Collection<Emprunt> getEmpruntCollection() {
        return empruntCollection;
    }

    public void setEmpruntCollection(Collection<Emprunt> empruntCollection) {
        this.empruntCollection = empruntCollection;
    }

    public Etat getEtatId() {
        return etatId;
    }

    public void setEtatId(Etat etatId) {
        this.etatId = etatId;
    }

    public Genre getGenreId() {
        return genreId;
    }

    public void setGenreId(Genre genreId) {
        this.genreId = genreId;
    }

    public Type getTypeId() {
        return typeId;
    }

    public void setTypeId(Type typeId) {
        this.typeId = typeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocument != null ? idDocument.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Document)) {
            return false;
        }
        Document other = (Document) object;
        if ((this.idDocument == null && other.idDocument != null) || (this.idDocument != null && !this.idDocument.equals(other.idDocument))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Document[ idDocument=" + idDocument + " ]";
    }
    
}
