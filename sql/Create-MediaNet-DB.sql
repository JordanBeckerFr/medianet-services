CREATE TABLE ADHERENT (
		ID_ADHERENT INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM VARCHAR(255),
        PRENOM VARCHAR(255),
        MAIL VARCHAR(255),
        DATE_ADHESION DATE,
        ADRESSE VARCHAR(255),
        PRIMARY KEY (ID_ADHERENT));

CREATE TABLE ETAT (
		ID_ETAT INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM_ETAT VARCHAR(255),
        DESCRIPTION_ETAT VARCHAR(255),
        PRIMARY KEY (ID_ETAT));

CREATE TABLE GENRE (
		ID_GENRE INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM_GENRE VARCHAR(255),
        PRIMARY KEY (ID_GENRE));

CREATE TABLE TYPE (
		ID_TYPE INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM_TYPE VARCHAR(255),
        PRIMARY KEY (ID_TYPE));

CREATE TABLE DOCUMENT (
		ID_DOCUMENT INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
		TITRE VARCHAR(255),
		AUTEUR VARCHAR(255),
		DESCRIPTION_DOCUMENT VARCHAR(255),
        URL VARCHAR(255),
        DATE_AJOUT DATE,
        DATE_PUBLICATION DATE,
        ETAT_ID INTEGER NOT NULL,
        GENRE_ID INTEGER NOT NULL,
        TYPE_ID INTEGER NOT NULL,
        PRIMARY KEY (ID_DOCUMENT),
        Foreign Key (ETAT_ID) REFERENCES ETAT(ID_ETAT),
        Foreign Key (GENRE_ID) REFERENCES GENRE(ID_GENRE),
        Foreign Key (TYPE_ID) REFERENCES TYPE(ID_TYPE));

CREATE TABLE EMPRUNT (
		ID_EMPRUNT INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        DATE_EMPRUNT DATE,
        DATE_RETOUR DATE,
        ADHERENT_ID INTEGER NOT NULL,
        DOCUMENT_ID INTEGER NOT NULL,
        PRIMARY KEY (ID_EMPRUNT),
        Foreign Key (ADHERENT_ID) REFERENCES ADHERENT(ID_ADHERENT),
        Foreign Key (DOCUMENT_ID) REFERENCES DOCUMENT(ID_DOCUMENT));

INSERT INTO ADHERENT (NOM, PRENOM, MAIL, DATE_ADHESION, ADRESSE) 
        VALUES ('AAA', 'Aaa', 'AAA@AAA.COM', '2000-01-10', 'AAAAADRESSE');
INSERT INTO ADHERENT (NOM, PRENOM, MAIL, DATE_ADHESION, ADRESSE) 
        VALUES ('BBB', 'Bbb', 'BBB@BBB.COM', '2000-01-11', 'BBBBBDRESSE');
INSERT INTO ETAT (NOM_ETAT) 
        VALUES ('Disponible');
INSERT INTO ETAT (NOM_ETAT) 
        VALUES ('Emprunté');
INSERT INTO ETAT (NOM_ETAT, DESCRIPTION_ETAT) 
        VALUES ('Indisponible', 'K C');
INSERT INTO GENRE (NOM_GENRE) 
        VALUES ('Romance');
INSERT INTO GENRE (NOM_GENRE) 
        VALUES ('Comédie');
INSERT INTO GENRE (NOM_GENRE) 
        VALUES ('Action');
INSERT INTO TYPE (NOM_TYPE) 
        VALUES ('Livre');
INSERT INTO TYPE (NOM_TYPE) 
        VALUES ('CD Audio');
INSERT INTO TYPE (NOM_TYPE) 
        VALUES ('DVD Vidéo');
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION_DOCUMENT, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('LLL1', 'AUT1', 'DESC1', 'http://www.placecage.com/200/300','2000-01-15', '2000-01-10', 1, 1, 1);
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION_DOCUMENT, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('LLL2', 'AUT2', 'DESC2', 'http://www.placecage.com/g/200/300', '2000-01-16', '2000-01-11', 3, 2, 2);
INSERT INTO EMPRUNT (DATE_EMPRUNT, DATE_RETOUR, ADHERENT_ID, DOCUMENT_ID) 
        VALUES ('2000-01-17', '2000-01-28', 1, 1);
INSERT INTO EMPRUNT (DATE_EMPRUNT, DATE_RETOUR, ADHERENT_ID, DOCUMENT_ID) 
        VALUES ('2000-01-18', '2000-01-29', 2, 2);
